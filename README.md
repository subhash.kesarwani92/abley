# Abley test with React App

- **Initial Project Launch:** 22 October 2021

## Abley test Overview

### Application Information

Create a simple Typescript application using the create react app tool and the ESRI JavaScript API to consume spatial data and display some information on a map.



## Technology Used

React() typescript hooks based functional components

### Unit Testing

The project uses Jest and Enzyme for unit testing. The target is 80% line, statement and branch coverage.

#### Jest Resources
- [Documentation](https://jestjs.io/docs/en/getting-started)
- [Cheat sheet](https://devhints.io/jest)

#### Enzyme Resources

- [Documentation](https://airbnb.io/enzyme/)
- [Cheat sheet](https://devhints.io/enzyme)

### Bootstrap

version:5.1.3


### Api Package

ArcGIS API for JavaScript(https://developers.arcgis.com/javascript/latest/)

## Project Set-up

### Install packages
`npm install`

### Run project
`npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### Run unit test cases
`npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### Build project
`npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.
