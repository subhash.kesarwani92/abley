import React from "react";
import { shallow } from "enzyme";
import MapPage from "../index";

function resetAll() {}

beforeEach(resetAll);
afterEach(resetAll);

/** Component renders correctly */
test("Component renders correctly", async () => {
	const item = shallow(<MapPage />);
	expect(item.find("div").length).toBe(2);
});
