import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import Map from "@arcgis/core/Map";
import MapView from "@arcgis/core/views/MapView";
import config from "@arcgis/core/config";
import ScaleBar from "@arcgis/core/widgets/ScaleBar";
import Compass from "@arcgis/core/widgets/Compass";
import { useSelector } from "react-redux";
import { Spinner } from "reactstrap";

export interface IMapProps {
	layer?: any;
}
const MapPage: React.FC<IMapProps> = (props: any) => {
	const [loading, setLoading] = useState(false);
	const getBaseMap = useSelector((data: any) => data.baseMapReducer);

	const location = useLocation();
	config.apiKey = "AAPK96e626e9b5dc4ae5bd7b1d895ccf540fDGmRdx3CJ6ePfnO7bZyGjxf03Xiere3WlGfvN7U_tSjo-WkNW-S6FPyj55DLY6gw";
	config.geoRSSServiceUrl = "https://utility.arcgis.com/sharing/rss";

	const createMap = () => {
		setLoading(false);
		const map = new Map({
			basemap: getBaseMap,
		});

		const view: any = new MapView({
			map,
			center: [1204885, 72.5447753],
			zoom: 3,
			container: "viewDiv",
			popup: {
				autoOpenEnabled: false,
			},
		});

		view.on("layerview-create", (event: any) => {
			setLoading(true);
		});

		if (location.pathname === "/scale-bar") {
			let scaleBar = new ScaleBar({ view });
			view.ui.add(scaleBar, {
				position: "bottom-left",
			});
		}

		if (location.pathname === "/compass") {
			let compass = new Compass({ view });
			view.ui.add(compass, "top-left");
		}

		props.layer && map.add(props.layer);
	};

	useEffect(() => {
		createMap();
	}, [getBaseMap, props.layer, location]);

	return (
		<>
			<div className="max__height spinner__wrapper">
				{!loading && <Spinner />}
				<div id="viewDiv"></div>
			</div>
		</>
	);
};

export default MapPage;
