import React from "react";
import { shallow } from "enzyme";
import LayerListPage from "../index";

function resetAll() {}

beforeEach(resetAll);
afterEach(resetAll);

/** Component renders correctly */
test("Component renders correctly", async () => {
	const item = shallow(<LayerListPage />);
	expect(item.find("MapPage").length).toBe(1);
});
