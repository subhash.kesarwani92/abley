import React from "react";
import MapPage from "../MapPage";
import { useSelector } from "react-redux";
import FeatureLayer from "@arcgis/core/layers/FeatureLayer";
import OGCFeatureLayer from "@arcgis/core/layers/OGCFeatureLayer";
import GeoRSSLayer from "@arcgis/core/layers/GeoRSSLayer";
const LayerListPage: React.FC = (props) => {
	const selectLayers = useSelector((data: any) => data.layersReducer);
	let layer = null;
	switch (selectLayers) {
		case "feature-layer":
			layer = new FeatureLayer({
				url: "https://sampleserver6.arcgisonline.com/arcgis/rest/services/Census/MapServer/3",
			});
			break;
		case "csv-layer":
			layer = new OGCFeatureLayer({
				url: "https://vtp2.geo-solutions.it/geoserver/ogc/features",
				collectionId: "ne:countries50m",
			});
			break;
		case "geo-rss-layer":
			layer = new GeoRSSLayer({
				url: "https://disasterscharter.org/charter-portlets/cpi-mvc/activations/feed/rss/",
			});
			break;
		default:
			break;
	}
	return (
		<>
			<MapPage layer={layer} />
		</>
	);
};

export default LayerListPage;
