import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.scss";
import Header from "./components/Header";
import Footer from "./components/Footer";
import { Container } from "reactstrap";
import LayerListPage from "./pages/LayerListPage";
import MapPage from "./pages/MapPage";

const App = () => {
	return (
		<Container fluid>
			<Router>
				<Header />
				<Switch>
					<Route exact path="/">
						<MapPage />
					</Route>
					<Route exact path="/compass">
						<MapPage />
					</Route>
					<Route exact path="/layer-list">
						<LayerListPage />
					</Route>
					<Route exact path="/scale-bar">
						<MapPage />
					</Route>
				</Switch>
			</Router>
			<Footer />
		</Container>
	);
};

export default App;
