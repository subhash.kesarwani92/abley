import React from "react";
import { Navbar, NavbarBrand } from "reactstrap";

const Footer: React.FC = () => {
	return (
		<div className="footer">
			<Navbar className="navbar" color="light" light expand="md">
				<NavbarBrand>Copyright © {new Date().getFullYear()} Abley. All rights reserved.</NavbarBrand>
			</Navbar>
		</div>
	);
};

export default Footer;
