import React from "react";
import { shallow } from "enzyme";
import Footer from "../index";

function resetAll() {}

beforeEach(resetAll);
afterEach(resetAll);

/** Component renders correctly */
test("Component renders correctly", async () => {
	const item = shallow(<Footer />);
	expect(item.find("Navbar").length).toBe(1);
	expect(item.find("NavbarBrand").length).toBe(1);
});
