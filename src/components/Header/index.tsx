import React from "react";
import { useHistory } from "react-router-dom";
import {
    Navbar,
    Nav,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    NavItem,
    NavLink,
} from "reactstrap";
import { Basemap } from "../../store/action/action";
import { Layers } from "../../store/action/action";

import { useDispatch } from "react-redux";

const Header: React.FC = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const handleBaseMap = (e: any) => {
        dispatch(Basemap(e.target.value));
    };

    const handleLayers = (e: any) => {
        history.push("/layer-list");
        dispatch(Layers(e.target.value));
    };

    const baseMapTypeArray = [
        "arcgis-imagery",
        "arcgis-imagery-standard",
        "arcgis-imagery-labels",
        "arcgis-light-gray",
        "arcgis-dark-gray",
        "arcgis-navigation",
        "arcgis-navigation-night",
        "arcgis-streets",
        "arcgis-streets-night",
        "arcgis-streets-relief",
        "arcgis-topographic",
        "arcgis-oceans",
        "arcgis-terrain",
        "arcgis-community",
        "arcgis-charted-territory",
        "arcgis-colored-pencil",
        "arcgis-nova",
        "arcgis-modern-antique",
        "arcgis-midcentury",
        "arcgis-newspaper",
        "arcgis-hillshade-light",
        "arcgis-hillshade-dark",
    ];

    const layersArray = ["feature-layer", "csv-layer", "geo-rss-layer"];

    return (
        <>
            <Navbar color="light" light expand="md">
                <Nav className="mr-auto" navbar>
                    <NavItem role="button">
                        <NavLink onClick={(e) => history.push("/")}>
                            Home
                        </NavLink>
                    </NavItem>
                    <NavItem role="button">
                        <NavLink onClick={(e) => history.push("/compass")}>
                            Compass
                        </NavLink>
                    </NavItem>
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle nav caret>
                            Layers
                        </DropdownToggle>
                        <DropdownMenu
                            data-testid="layers"
                            onClick={(e) => handleLayers(e)}
                            right
                        >
                            {layersArray.map((item, index) => (
                                <DropdownItem
                                    key={index}
                                    className="basemap__title"
                                    value={item}
                                >
                                    {item.replace(/-/, " ")}
                                </DropdownItem>
                            ))}
                        </DropdownMenu>
                    </UncontrolledDropdown>
                    <NavItem role="button">
                        <NavLink onClick={(e) => history.push("/scale-bar")}>
                            Scalebar
                        </NavLink>
                    </NavItem>
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle nav caret>
                            Base map
                        </DropdownToggle>
                        <DropdownMenu
                            data-testid="base-map"
                            onClick={(e) => handleBaseMap(e)}
                            right
                        >
                            {baseMapTypeArray.map((item, index) => (
                                <DropdownItem
                                    key={index}
                                    className="basemap__title"
                                    value={item}
                                >
                                    {item.replace(/-/, " ")}
                                </DropdownItem>
                            ))}
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </Nav>
            </Navbar>
        </>
    );
};

export default Header;
