import React from "react";
import Header from "../index";

import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { fireEvent, render, screen } from "@testing-library/react";

function resetAll() {}

const mockHistoryPush = jest.fn();

jest.mock("react-router-dom", () => ({
    ...jest.requireActual("react-router-dom"),
    useHistory: () => ({
        push: mockHistoryPush,
    }),
}));

jest.mock("@arcgis/core/Map", () => {
    return () => jest.fn();
});

beforeEach(resetAll);
afterEach(resetAll);

/** Component renders correctly */
test("Component renders correctly", async () => {
    const initialState = { output: 10 };
    const mockStore = configureStore();

    let store = mockStore(initialState);

    const item = render(
        <Provider store={store}>
            <Header />
        </Provider>
    );

    expect(item).not.toBe(null);
});

test("Component renders correctly with navbar items", async () => {
    const initialState = { output: 10 };
    const mockStore = configureStore();

    let store = mockStore(initialState);

    render(
        <Provider store={store}>
            <Header />
        </Provider>
    );

    const navBar = screen.findByRole("Navbar");
    expect(navBar).not.toBe(null);
    screen.findByRole("NavItem");
});

test("User move on homepage", async () => {
    const initialState = { output: 10 };
    const mockStore = configureStore();

    let store = mockStore(initialState);

    const item = render(
        <Provider store={store}>
            <Header />
        </Provider>
    );
    fireEvent.click(screen.getByText("Home"));
    expect(mockHistoryPush).toHaveBeenCalledWith("/");
});

test("User move on Compass", async () => {
    const initialState = { output: 10 };
    const mockStore = configureStore();

    let store = mockStore(initialState);

    const item = render(
        <Provider store={store}>
            <Header />
        </Provider>
    );
    fireEvent.click(screen.getByText("Compass"));
    expect(mockHistoryPush).toHaveBeenCalledWith("/compass");
});

test("User clicks on Layers and see dropdown", async () => {
    const initialState = { output: 10 };
    const mockStore = configureStore();

    let store = mockStore(initialState);

    const item = render(
        <Provider store={store}>
            <Header />
        </Provider>
    );
    fireEvent.click(screen.getByTestId("layers"));
    expect(mockHistoryPush).toHaveBeenCalledWith("/layer-list");
});

test("User clicks on Scalebar", async () => {
    const initialState = { output: 10 };
    const mockStore = configureStore();

    let store = mockStore(initialState);

    const item = render(
        <Provider store={store}>
            <Header />
        </Provider>
    );
    fireEvent.click(screen.getByText("Scalebar"));
    expect(mockHistoryPush).toHaveBeenCalledWith("/scale-bar");
});

test("User clicks on Base map", async () => {
    const initialState = { output: 10 };
    const mockStore = configureStore();

    let store = mockStore(initialState);

    const item = render(
        <Provider store={store}>
            <Header />
        </Provider>
    );
    fireEvent.click(screen.getByTestId("base-map"));
});
