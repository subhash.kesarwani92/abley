import { combineReducers } from "redux";
const baseMapReducer = (state = "streets", action: any) => {
	switch (action.type) {
		case "Basemap":
			return action.payload;

		default:
			return state;
	}
};
const layersReducer = (state = null, action: any) => {
	switch (action.type) {
		case "Layers":
			return action.payload;

		default:
			return state;
	}
};

const allReducers = combineReducers({
	baseMapReducer,
	layersReducer,
});
export default allReducers;
