export const Basemap = (data: any) => {
	return {
		type: "Basemap",

		payload: data,
	};
};
export const Layers = (data: any) => {
	return {
		type: "Layers",

		payload: data,
	};
};
